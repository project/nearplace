<?php

namespace Drupal\Nearplace\Plugin\Block;

use Drupal\Core\Block\BlockBase;
use Drupal\Core\Block\BlockPluginInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Provides a 'Nearplace' Block.
 *
 * @Block(
 *   id = "nearplace",
 *   admin_label = @Translation("Nearplace block"),
 * )
 */

class NearplaceBlock extends BlockBase implements BlockPluginInterface
{

    public function build() {
        $config = $this->getConfiguration();
        $locatorCode = '';
        if (!empty($config['locator_code'])) {
            $locatorCode = $config['locator_code'];
        }

        return array(
            '#markup' => $this->t($locatorCode),
        );
    }

    /**
     * {@inheritdoc}
     */
    public function blockForm($form, FormStateInterface $form_state) {
        $form = parent::blockForm($form, $form_state);

        $config = $this->getConfiguration();

        $form['nearplace_block_organization_uuid'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Organization UUID'),
            '#description' => $this->t('Input your Nearplace organization\'s UUID'),
            '#default_value' => isset($config['organization_uuid']) ? $config['organization_uuid'] : '',
        );

        $form['nearplace_block_locator_uuid'] = array(
            '#type' => 'textfield',
            '#title' => $this->t('Locator UUID'),
            '#description' => $this->t('Input your Nearplace locator\'s UUID'),
            '#default_value' => isset($config['locator_uuid']) ? $config['locator_uuid'] : '',
        );

        return $form;
    }

    /**
     * {@inheritdoc}
     */
    public function blockSubmit($form, FormStateInterface $form_state) {
        $organizationUuid = $form_state->getValue('nearplace_block_organization_uuid');
        $locatorUuid = $form_state->getValue('nearplace_block_locator_uuid');
        $this->configuration['organization_uuid'] = $organizationUuid;
        $this->configuration['locator_uuid'] = $locatorUuid;
        try {
            $response = json_decode(file_get_contents($this->getLocatorUrl($locatorUuid, $organizationUuid)));
            $this->configuration['locator_code'] = $response->data->script->data->content;
        } catch (\Exception $e) {
            $this->configuration['locator_code'] = 'exception';
        }
    }

    private function getLocatorUrl($locatorUuid, $organizationUuid)
    {
        $query = http_build_query([
            'organizationUuid' => $organizationUuid,
            'locatorUuid' => $locatorUuid,
            'includes' => ['script']
        ]);
        return 'http://api.nearplace.com/v1/locators/'.$locatorUuid.'?'.$query;
    }
}